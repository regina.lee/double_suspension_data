% close all; 
clear all;
double_pendulum_c4;
%% Load state spaces
sys = ss(A,B,C,D);
sysV = ss(Av,Bv,Cv,Dv);
sysY = ss(Ay,By,Cy,Dy);
sysTR = ss(Atr,Btr,Ctr,Dtr);
%% Input Torque L/P Bode
hold on
% figure();

options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';
options.XLabel.FontSize = 20;
options.YLabel.FontSize = 20;
options.Title.FontSize = 20;
options.TickLabel.FontSize = 20;
options.Title.String = '';

% figure('units','normalized','outerposition',[0 0 1 1])
bode(sys(1,4),sys(2,4),options)


xlim([0.001,4000])
title('Input of Force at Suspension Point to Top Mass Displacement')
xlabel('Frequency (Hz)');
legend('Torque-to-P','Torque-to-Length');
set(findall(gcf,'type','line'),'linewidth',3)
% plot_darkmode
%% PLOT L/P BODE
hold on
% figure();

options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';
options.XLabel.FontSize = 20;
options.YLabel.FontSize = 20;
options.Title.FontSize = 20;
options.TickLabel.FontSize = 20;
options.Title.String = '';
% bode(sys, options)

bode(sys(1,3),sys(2,3),options)
% bode(sys(1,1),sys(2,1),options)

% legend('L-to-P','L-to-L','P-to-P');
xlim([0.1,40])
title('Input of Displacement at Suspension Point to Top Mass Displacement - 4 Wires')
xlabel('Frequency (Hz)');
legend('L-to-P','L-to-L');
set(findall(gcf,'type','line'),'linewidth',3)
% set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
% plot_darkmode;

% figure();
% bode(sys(1,3), sys(2,3),options)
% legend('Length F-to-P','Length F-to-L');
% xlim([0.1,40])
% title('Input of Force at COM');
%% P/P Bode
% figure()
options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';
options.XLabel.FontSize = 20;
options.YLabel.FontSize = 20;
options.Title.FontSize = 20;
options.TickLabel.FontSize = 20;
options.Title.String = '';
bode(sys(1,5),options);
% legend('P to P on Top Mass','P to P on Bottom Mass');
% title('Input of Angle at Suspension Point to Top Mass Displacement - 4 Wires')
xlim([0.1,40])
xlabel('Frequency (Hz)');
set(findall(gcf,'type','line'),'linewidth',2)
grid on;
%% Plot Vertical Bode

options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';
bode(sysV(1,2), options)
set(findall(gcf,'type','line'),'linewidth',3)


%% Plot Yaw Bode

options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';
set(findall(gcf,'type','line'),'linewidth',3)

bode(sysY(1,1), options)
set(findall(gcf,'type','line'),'linewidth',3)

  
%% Plot Trans/Roll Bode

options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';
options.XLabel.FontSize = 20;
options.YLabel.FontSize = 20;
options.Title.FontSize = 20;
options.TickLabel.FontSize = 20;
options.Title.String = '';
bode(sysTR(1,3),sysTR(2,3), options)
legend('T-to-R','T-to-T')
title('Input of transverse force - configuration 4')
set(findall(gcf,'type','line'),'linewidth',3)


%% Plot all diagonal to top mass
options = bodeoptions;
options.FreqUnits = 'Hz'; 
options.MagUnits = 'abs';
options.MagScale = 'log';
options.Grid = 'on';

bode(sys(2,3),sys(1,5),sysY(1,1),sysTR(1,2),sysTR(2,3),sysV(1,2),options)
xlim([0.1,100]);
legend('L-to-L','P-to-P,','Y-to-Y','R-to-R','T-to-T','V-to-V');
set(findall(gcf,'type','line'),'linewidth',3)
