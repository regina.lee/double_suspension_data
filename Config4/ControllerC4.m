close all; clear all;
DataLoadConfig4;
%% L to L Fit
s = tf('s');
p1 = 0.6875*2*pi;
p2 = 1.59375*2*pi;
z1 = 0.9375*2*pi;
zz1 = 0.01; 
zp1 = 0.01;
zp2 = 0.01;
k = 52.1053;

H = k*(s^2+2*z1*zz1*s+z1^2)/((s^2+2*p1*zp1*s+p1^2)*(s^2+2*zp2*p2*s+p2^2));
[mag, phase, wout] = bode(H);

figure();
subplot(2,1,1);
loglog(wout/2/pi, squeeze(mag),'-.','MarkerSize',2,'LineWidth',3);
hold on
loglog(fL, MagL2L,'-o','MarkerSize',2,'LineWidth',3);
xlim([0.1,100])
grid on;

subplot(2,1,2);
semilogx(wout/2/pi, squeeze(phase),'-.','LineWidth',3)
hold on
semilogx(fL, PhL2L, '-o', 'MarkerSize', 2,'LineWidth',3)
xlim([0.1,100])
grid on;

legend('Fit', 'Experimental')
%% L to L Controller
opt = bodeoptions;
opt.Grid = 'on';
opt.FreqUnits = 'Hz';
opt.MagUnits = 'abs';
opt.MagScale = 'log';
%opt.XLim = [0.1 50];
opt.XLabel.FontSize = 18;
opt.YLabel.FontSize = 18;
opt.TickLabel.FontSize = 16;
opt.Title.String = '';
opt.XLabel.Interpreter = 'latex';
opt.YLabel.Interpreter  = 'latex';

g = 1;
z_lead = 0;
p_lead = 1.8;%2.1;%1.8438;%2.83;
f_unityG = 1.82;%2.2;%1.94;%2.85;
f_cc = 20;
angle_cc = 60;
p_lead2 = 20;

[cc, g_cc] = complex_conj(f_cc,angle_cc);

% Con = zpk(-2*pi*[z_lead, f_cc], -2*pi*[p_lead, -cc(1)/(2*pi), -cc(2)/(2*pi)], 1);
Con = zpk(-2*pi*[z_lead], -2*pi*[p_lead, p_lead2], 1);
Con = Con/abs(freqresp(Con*H,2*pi*f_unityG));

OL = Con*H;
CL = 1/(1+OL);
% CLp = CL*sys(2,2);
CLp = H/(1+(Con*H));
SenseNoise = Con/(1+OL);

pp = pole(CL);
flag = any(real(pp)>0);

figure
bode(H, Con, OL, CLp, opt)
legend('Plant', 'Controller', 'OL', 'CL')
grid on
set(findall(gcf,'type','line'),'linewidth',2)

figure
margin(OL)
set(findall(gcf,'type','line'),'linewidth',2)

figure
bode(H, CLp, opt)
legend('Plant', 'Close-loop plant')
grid on
set(findall(gcf,'type','line'),'linewidth',2)

% figure()
% bode(CL,opt)
% grid on
% title('Suppresion')
% set(findall(gcf,'type','line'),'linewidth',2)
% 
% figure()
% bode(SenseNoise,opt);
% grid on
% title('Sensor Noise TF');
% set(findall(gcf,'type','line'),'linewidth',2)
if flag
    set(gcf, 'color', [1 0 0]);
end

function [result,g]= complex_conj(freq,angle)
    z=2*pi*freq*exp(j*angle*pi/180);
    result= [-z -conj(z)];
    g=abs(result(1))^2;
end