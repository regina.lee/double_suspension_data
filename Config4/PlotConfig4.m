clear all;close all;
DataLoadConfig4;

%% Length Input (Pitch and Length Output)
% figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(wModelInFL/2/pi,squeeze(MagModelInFL),'LineWidth',3); % L to L Model
% loglog(wModelInF/2/pi,squeeze(MagModelInF),'LineWidth',3); % L to P Model
% loglog(wModelInT/2/pi,squeeze(MagModelInT),'LineWidth',3); % Torque to P Model
hold on
loglog(fL,MagL2L,'-o','LineWidth',3,'MarkerSize',2); %L to P Experimental
% loglog(woutL/2/pi,normalize(MagSimL,'norm'),'LineWidth',3); %L to L Simulation
% loglog(fL,MagL2L*1/ct2N,'-.','LineWidth',3); %L to L Experimental


xlim([0.1,100]);
% title('Input Length to Top Mass')
title('Comparison of State Space Model and Experimental Data Top Mass');
% ylabel('Magnitude (\mum/counts, \murad/counts)')
ylabel('Magnitude (Disp (\phi)/Force (N))')
grid on;
% set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
set(findall(gcf,'-property','FontSize'),'FontSize',20)
% plot_darkmode


subplot(2,1,2);
semilogx(wModelInFL/2/pi,squeeze(PhModelInFL),'LineWidth',3); % L to L Model
% semilogx(wModelInF/2/pi,squeeze(PhModelInF),'LineWidth',3); % L to P Model
% semilogx(wModelInT/2/pi,squeeze(PhModelInT),'LineWidth',3); % Torque to P Model
hold on;
% semilogx(fL,PhL2P,'-o','LineWidth',3,'MarkerSize',2); % L to P Experimental 
% semilogx(woutL/2/pi,PhSimL,'LineWidth',3); %L to L Simulation
semilogx(fL,PhL2L,'-.','LineWidth',3); % L to L Experimental

grid on;
ylim([-800,400]);
xlim([0.1,100]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
% legend('Length/Length','Pitch/Length');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('Force-to-P State Space Model','Force-to-P Experimental');

% plot_darkmode

%% All length input
figure;
subplot(2,1,1);
loglog(fL,MagL2L);
hold on
loglog(fL,MagL2P);
% loglog(fL,MagL2R);
% loglog(fL,MagL2T);
% loglog(fL,MagL2V);
% loglog(fL,MagL2Y);
title('Input Length to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
xlim([0.1,70])
% plot_darkmode

subplot(2,1,2);
semilogx(fL,PhL2L);
hold on;
semilogx(fL,PhL2P);
% semilogx(fL,PhL2R);
% semilogx(fL,PhL2T);
% semilogx(fL,PhL2V);
% semilogx(fL,PhL2Y);
ylim([-1000,200]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
xlim([0.1,70])
grid on;
% legend('L-to-L','L-to-P','L-to-R','L-to-T','L-to-V','L-to-Y');
legend('L-to-L','L-to-P');
% plot_darkmode;

%% Bottom Mass Length Input
figure;
subplot(2,1,1);
loglog(fL,MagL2PS,'-.','LineWidth',3); %L to PS Experimental
% loglog(fL,normalize(MagL2OpP, 'norm'),'LineWidth',3); %L to Op Lev P Experimental
hold on
% loglog(fL,MagL2OpY,'LineWidth',3); %L to Op Lev Y Experimental
loglog(fL,MagL2OpP,'-.','LineWidth',3); %L to Op Lev P Experimental
% loglog(wModelLLB/2/pi, normalize(MagModelLLB,'norm'), 'LineWidth',3);
loglog(wModelInFL/2/pi, MagModelInFL, 'LineWidth',3);
loglog(wModelInF/2/pi, MagModelInF, 'LineWidth',3);
xlim([0.1,10]);
% title('Input Length to Top Mass')
title('Length Input to Bottom Mass Comparison');
ylabel('Magnitude (\phi/F)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
grid on;
% plot_darkmode

subplot(2,1,2);
semilogx(fL,PhL2PS,'-.','LineWidth',3); % L to PS Experimental
% semilogx(fL,PhL2OpP,'LineWidth',3); % L to Op Lev P Experimental 
hold on;
% semilogx(fL,PhL2OpY,'LineWidth',3); % L to Op Lev Y Experimental 
semilogx(fL,PhL2OpP,'-.','LineWidth',3); % L to Op Lev P Experimental 
% semilogx(wModelLLB/2/pi, PhModelLLB, 'LineWidth',3);
semilogx(wModelInFL/2/pi, PhModelInFL, 'LineWidth',3);
semilogx(wModelInF/2/pi, PhModelInF, 'LineWidth',3);
grid on;
ylim([-800,400]);
xlim([0.1,10]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
% legend('Length/Length','Pitch/Length');
legend( 'Force L-to-L (Experimental)','Force L-to-P (Experimental)',...
    'Force L-to-L (Model)','Force L-to-P (Model)');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
% plot_darkmode

%% Pitch Input
figure;
subplot(2,1,1);

loglog(fP,MagP2P,'MarkerSize',2);
hold on
loglog(fP,MagP2L,'-.');
loglog(wout/2/pi,MagSim,'-.');
xlim([0.1,30]);
plot_darkmode

title('Input Pitch to Top Mass')
ylabel('Mag [\mum/cts, \murad/cts]')

grid on;
subplot(2,1,2);
semilogx(fP,PhP2P);
hold on;
semilogx(fP,PhP2L);
semilogx(wout/2/pi,PhSim,'-.');
legend('Pitch/Pitch','Length/Pitch','Simulated P/P');
plot_darkmode
grid on;
ylim([-400,300]);
xlim([0.1,30]);

ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
%% All Pitch Input
figure;
subplot(2,1,1);
loglog(fP,MagP2L);
hold on
loglog(fP,MagP2P);
loglog(fP,MagP2R);
loglog(fP,MagP2T);
loglog(fP,MagP2V);
loglog(fP,MagP2Y);
title('Input Pitch to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,30]);
grid on;
% plot_darkmode

subplot(2,1,2);
semilogx(fP,PhP2L);
hold on;
semilogx(fP,PhP2P);
semilogx(fP,PhP2R);
semilogx(fP,PhP2T);
semilogx(fP,PhP2V);
semilogx(fP,PhP2Y);
ylim([-800,400]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
xlim([0.1,30]);
grid on;
legend('P-to-L','P-to-P','P-to-R','P-to-T','P-to-V','P-to-Y');
% plot_darkmode;
%% Roll Input
figure;
subplot(2,1,1);
loglog(fR,MagR2L);
hold on
loglog(fR,MagR2P);
loglog(fR,MagR2R);
loglog(fR,MagR2T);
loglog(fR,MagR2V);
loglog(fR,MagR2Y);
title('Input Roll to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,20]);
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fR,PhR2L);
hold on;
semilogx(fR,PhR2P);
semilogx(fR,PhR2R);
semilogx(fR,PhR2T);
semilogx(fR,PhR2V);
semilogx(fR,PhR2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('R-to-L','R-to-P','R-to-R','R-to-T','R-to-V','R-to-Y');
plot_darkmode;
%% Trans Input
figure;
subplot(2,1,1);
loglog(fT,MagT2L);
hold on
loglog(fT,MagT2P);
loglog(fT,MagT2R);
loglog(fT,MagT2T);
loglog(fT,MagT2V);
loglog(fT,MagT2Y);
title('Input Transverse to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,20]);
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fT,PhT2L);
hold on;
semilogx(fT,PhT2P);
semilogx(fT,PhT2R);
semilogx(fT,PhT2T);
semilogx(fT,PhT2V);
semilogx(fT,PhT2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('T-to-L','T-to-P','T-to-R','T-to-T','T-to-V','T-to-Y');
plot_darkmode;

%% Vert Input
figure;
subplot(2,1,1);
loglog(fV,MagV2L);
hold on
loglog(fV,MagV2P);
% loglog(fV,MagV2R);
% loglog(fV,MagV2T);
% loglog(fV,MagV2V);
% loglog(fV,MagV2Y);
title('Input Vertical to Top Mass')
xlim([0.1,20]);
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
% plot_darkmode

subplot(2,1,2);
semilogx(fV,PhV2L);
hold on;
semilogx(fV,PhV2P);
% semilogx(fV,PhV2R);
% semilogx(fV,PhV2T);
% semilogx(fV,PhV2V);
% semilogx(fV,PhV2Y);
ylim([-1000,200]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('V-to-L','V-to-P');
% legend('V-to-L','V-to-P','V-to-R','V-to-T','V-to-V','V-to-Y');
% plot_darkmode;
set(findall(gcf,'type','line'),'linewidth',3)
%% Yaw Input
figure;
subplot(2,1,1);
loglog(fY,MagY2L);
hold on
loglog(fY,MagY2P);
loglog(fY,MagY2R);
loglog(fY,MagY2T);
loglog(fY,MagY2V);
loglog(fY,MagY2Y);
xlim([0.1,20]);
title('Input Yaw to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fY,PhY2L);
hold on;
semilogx(fY,PhY2P);
semilogx(fY,PhY2R);
semilogx(fY,PhY2T);
semilogx(fY,PhY2V);
semilogx(fY,PhY2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('Y-to-L','Y-to-P','Y-to-R','Y-to-T','Y-to-V','Y-to-Y');
plot_darkmode;
%% All diagonal TFs to top mass
figure;
subplot(2,1,1);
loglog(fL,MagL2L);
hold on
loglog(fP, MagP2P);
loglog(fY, MagY2Y);
loglog(fR, MagR2R);
loglog(fT, MagT2T);
loglog(fV, MagV2V);
loglog(fL, MagL2P);
xlim([0.1,100]);

title('Diagonal Transfer Functions - Config 4')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;

subplot(2,1,2);
semilogx(fL,PhL2L);
hold on
semilogx(fP, PhP2P);
semilogx(fY, PhY2Y);
semilogx(fR, PhR2R);
semilogx(fT, PhT2T);
semilogx(fV, PhV2V);
semilogx(fL, PhL2P);
grid on;
xlim([0.1,100]);
ylim([-800,400]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
legend('L-to-L','P-to-P','Y-to-Y','R-to-R','T-to-T','V-to-V');
set(findall(gcf,'type','line'),'linewidth',3)

%% Frame
figure();
loglog(framePowerSpectrum(:,1),framePowerSpectrum(:,2))
xlabel('Frequency (Hz)');
ylabel('Magnitude (m/\surd(Hz)');
title('Power Spectrum of OSEM on Frame')
grid on;
%% Translational Motions
figure;
subplot(2,1,1);
loglog(fL,MagL2L);
hold on
loglog(fT, MagT2T);
loglog(fV, MagV2V);
xlim([0.1,100]);

title('Input Force to Top Mass')
ylabel('Magnitude (\mum/cts)')
grid on;

subplot(2,1,2);
semilogx(fL,PhL2L);
hold on
semilogx(fT, PhT2T);
semilogx(fV, PhV2V);
grid on;
xlim([0.1,100]);
ylim([-800,400]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
legend('L-to-L','T-to-T','V-to-V');
set(findall(gcf,'type','line'),'linewidth',3)
%% Rotational Motions

figure;
subplot(2,1,1);
loglog(fP, MagP2P);
hold on
loglog(fY, MagY2Y);
loglog(fR, MagR2R);

xlim([0.1,100]);

title('Input Force to Top Mass')
ylabel('Magnitude (\murad/cts)')
grid on;

subplot(2,1,2);

semilogx(fP, PhP2P);
hold on
semilogx(fY, PhY2Y);
semilogx(fR, PhR2R);

grid on;
xlim([0.1,100]);
ylim([-800,400]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
legend('P-to-P','Y-to-Y','R-to-R');
set(findall(gcf,'type','line'),'linewidth',3)

