close all;
x = [12+7+5/32 12+7+1/8 12+6+31/32 12+7+3/32 12+7+1/16 12+7+7/32];
x = x.*0.0254;
m = [0 56 300 98 164 -40];
g= 9.8;
m=m.*g/1000;

scatter(x,m);


% Get coefficients of a line fit through the data.
coefficients = polyfit(x, m, 1);
% Create a new x axis with exactly 1000 points (or whatever you want).
xFit = linspace(min(x), max(x), 1000);
% Get the estimated yFit value for each of those 1000 new x locations.
yFit = polyval(coefficients , xFit);
% Plot everything.
scatter(x, m, 'b.'); % Plot training data.
hold on; % Set hold on so the next plot does not blow away the one we just drew.
plot(xFit, yFit, 'r-', 'LineWidth', 2); % Plot fitted line.
grid on;