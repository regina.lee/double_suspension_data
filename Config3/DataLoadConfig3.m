%% Load Data
% clear all; close all;
addpath('../Simulation Data');
% addpath('../vfit3');

rollLPRTV = load(fullfile('DataConfig3/RollDriveLPRTV'));
rollYPYPS1PS2 = load(fullfile('DataConfig3/RollDriveYOpPOpYPS1PS2'));
transLPRTV = load(fullfile('DataConfig3/TransDriveLPRTV'));
transYPYPS1PS2 = load(fullfile('DataConfig3/TransDriveYOpPOpYPS1PS2'));
lengthLPRTV = load(fullfile('DataConfig3/LongDriveLPRTV'));
lengthYPYPS1PS2 = load(fullfile('DataConfig3/LongDriveYOpPOpYPS1PS2'));
pitchLPRTV = load(fullfile('DataConfig3/PitchDriveLPRTV'));
pitchYPYPS1PS2 = load(fullfile('DataConfig3/PitchDriveYOpPOpYPS1PS2'));
vertLPRTV = load(fullfile('DataConfig3/VertDriveLPRTV'));
vertYPYPS1PS2 = load(fullfile('DataConfig3/VertDriveYOpPOpYPS1PS2'));
yawLPRTV = load(fullfile('DataConfig3/YawDriveLPRTV')); 
yawYPYPS = load(fullfile('DataConfig3/YawDriveYOpPOpYPS1PS2'));

DC5000LPRTV = load(fullfile('51023_Trial/LongDriveDC5000LPRTV'));
DC50000LPRTV = load(fullfile('51023_Trial/LongDriveDC50000LPRTV'));
DCNeg50000LPRTV = load(fullfile('51023_Trial/LongDriveDCNeg50000LPRTV'));
% testLPRTV = load(fullfile('51023_Trial/LongDriveLPRTV'));
testLPRTV = load(fullfile('DCExPeriment-OBS/LongDriveOrigLPRTV'));
% sim = load('G2.mat');
% DampCLOL = load(fullfile('DataConfig2/DampedCLOL'));
%% Model

double_pendulum_c3;
sys = ss(A,B,C,D);
% ct2N = 2^16/40*2*.0158; 
% ct2N = 2^16/40*2*.00608;
% ct2N = 2^16/40*2*.0198; 
ct2N = 2^16/40*2*.02; 
PitchCt2Rad = 40/2^16*1/2*1/58; %(40V/2^16cts)(1mm/2V)(1rad/58mm from cad)
PitchCt2N = PitchCt2Rad*1/0.61;
[MagModelLL, PhModelLL, wModelLL] = bode(-sys(2,1),{0.1,10000}); %In length, out length top mass
[MagModelLP, PhModelLP, wModelLP] = bode(-sys(1,1),{0.1,10000}); %In length, out pitch top mass
[MagModelLLB, PhModelLLB, wModelLLB] = bode(-sys(4,3),{0.1,10000}); %In length, out length bottom mass
[MagModelLPB, PhModelLPB, wModelLPB] = bode(-sys(3,3),{0.1,10000}); %In length, out pitch bottom mass
[MagModelInF, PhModelInF, wModelInF] = bode(-sys(1,3),{0.1,10000}); %Input force, output pitch top mass
[MagModelInFL, PhModelInFL, wModelInFL] = bode(sys(2,3),{0.1,1000}); % Input force, output length top mass
[MagModelInT, PhModelInT, wModelInT] = bode(-sys(1,4),{0.1,1000}); % Input force, output length top mass

sys2 = ss(Ay,By,Cy,Dy);
[MagModelYY, PhModelYY, wModelYY] = bode(sys2(1,1),{0.1,1000}); %In Yaw, out Yaw top mass
[MagModelYT, PhModelYT, wModelYT] = bode(sys(1,4)*sys2(1,1),{0.1,1000}); %In Yaw, out Yaw top mass

sys3 = ss(Atr, Btr, Ctr, Dtr);
[MagModelTR, PhModelTR, wModelTR] = bode(sys3(1,3),{0.1,1000}); %In Yaw, out Yaw top mass


%% Length Input
fL = lengthLPRTV(:,1);
[MagL2L,PhL2L] = getData(lengthLPRTV, 2, 3);
[MagL2P,PhL2P] = getData(lengthLPRTV, 4, 5);
[MagL2R,PhL2R] = getData(lengthLPRTV, 6, 7);
[MagL2T,PhL2T] = getData(lengthLPRTV, 8, 9);
[MagL2V,PhL2V] = getData(lengthLPRTV, 10, 11);

fL2 = lengthYPYPS1PS2(:,1);
[MagL2Y,PhL2Y] = getData(lengthYPYPS1PS2, 2, 3);
[MagL2OpP,PhL2OpP] = getData(lengthYPYPS1PS2, 4, 5);
[MagL2OpY,PhL2OpY] = getData(lengthYPYPS1PS2, 6, 7);
[MagL2PS1,PhL2PS1] = getData(lengthYPYPS1PS2, 8, 9);
[MagL2PS2,PhL2PS2] = getData(lengthYPYPS1PS2, 10, 11);

% [MagSimL,PhSimL,woutL] = bode(sim.G(6,1));
% MagSimL = squeeze(MagSimL);
% PhSimL = squeeze(PhSimL);
MagModelLL = squeeze(MagModelLL);
PhModelLL = squeeze(PhModelLL);
MagModelLP = squeeze(MagModelLP);
PhModelLP = squeeze(PhModelLP);
%% DC Experiment
fDC = DC5000LPRTV(:,1);
[MagL2LDC,PhL2LDC] = getData(DC5000LPRTV, 2, 3);
[MagL2PDC,PhL2PDC] = getData(DC5000LPRTV, 4, 5);
[MagL2RDC,PhL2RDC] = getData(DC5000LPRTV, 6, 7);
[MagL2TDC,PhL2TDC] = getData(DC5000LPRTV, 8, 9);
[MagL2VDC,PhL2VDC] = getData(DC5000LPRTV, 10, 11);

fDC2 = DC50000LPRTV(:,1);
[MagL2LDC2,PhL2LDC2] = getData(DC50000LPRTV, 2, 3);
[MagL2PDC2,PhL2PDC2] = getData(DC50000LPRTV, 4, 5);
[MagL2RDC2,PhL2RDC2] = getData(DC50000LPRTV, 6, 7);
[MagL2TDC2,PhL2TDC2] = getData(DC50000LPRTV, 8, 9);
[MagL2VDC2,PhL2VDC2] = getData(DC50000LPRTV, 10, 11);

fDC3 = DCNeg50000LPRTV(:,1);
[MagL2LDC3,PhL2LDC3] = getData(DCNeg50000LPRTV, 2, 3);
[MagL2PDC3,PhL2PDC3] = getData(DCNeg50000LPRTV, 4, 5);
[MagL2RDC3,PhL2RDC3] = getData(DCNeg50000LPRTV, 6, 7);
[MagL2TDC3,PhL2TDC3] = getData(DCNeg50000LPRTV, 8, 9);
[MagL2VDC3,PhL2VDC3] = getData(DCNeg50000LPRTV, 10, 11);

%% Test
ftest = testLPRTV(:,1);
[MagL2Ltest,PhL2Ltest] = getData(testLPRTV, 2, 3);
[MagL2Ptest,PhL2Ptest] = getData(testLPRTV, 4, 5);
[MagL2Rtest,PhL2Rtest] = getData(testLPRTV, 6, 7);
[MagL2Ttest,PhL2Ttest] = getData(testLPRTV, 8, 9);
[MagL2Vtest,PhL2Vtest] = getData(testLPRTV, 10, 11);

%% Pitch Input
fP = pitchLPRTV(:,1);

[MagP2L, PhP2L] = getData(pitchLPRTV, 2, 3);
[MagP2P, PhP2P] = getData(pitchLPRTV, 4, 5);
[MagP2R, PhP2R] = getData(pitchLPRTV, 6, 7);
[MagP2T, PhP2T] = getData(pitchLPRTV, 8, 9);
[MagP2V, PhP2V] = getData(pitchLPRTV, 10, 11);
[MagP2Y, PhP2Y] = getData(pitchYPYPS1PS2, 2, 3);

[MagP2OpP, PhP2OpP] = getData(pitchYPYPS1PS2, 4, 5);
[MagP2OpY, PhP2OpY] = getData(pitchYPYPS1PS2, 6, 7);
[MagP2PS1, PhP2PS1] = getData(pitchYPYPS1PS2, 8, 9);
[MagP2PS2, PhP2PS2] = getData(pitchYPYPS1PS2, 10, 11);




%% Roll Input
fR = rollLPRTV(:,1);
[MagR2L,PhR2L] = getData(rollLPRTV, 2, 3);
[MagR2P,PhR2P] = getData(rollLPRTV, 4, 5);
[MagR2R,PhR2R] = getData(rollLPRTV, 6, 7);
[MagR2T,PhR2T] = getData(rollLPRTV, 8, 9);
[MagR2V,PhR2V] = getData(rollLPRTV, 10, 11);
[MagR2Y,PhR2Y] = getData(rollYPYPS1PS2, 2, 3);

%% Trans Input
fT = transLPRTV(:,1);
[MagT2L,PhT2L] = getData(transLPRTV, 2, 3);
[MagT2P,PhT2P] = getData(transLPRTV, 4, 5);
[MagT2R,PhT2R] = getData(transLPRTV, 6, 7);
[MagT2T,PhT2T] = getData(transLPRTV, 8, 9);
[MagT2V,PhT2V] = getData(transLPRTV, 10, 11);
[MagT2Y,PhT2Y] = getData(transYPYPS1PS2, 2, 3);

%% Vert Input
fV = vertLPRTV(:,1);
[MagV2L,PhV2L] = getData(vertLPRTV, 2, 3);
[MagV2P,PhV2P] = getData(vertLPRTV, 4, 5);
[MagV2R,PhV2R] = getData(vertLPRTV, 6, 7);
[MagV2T,PhV2T] = getData(vertLPRTV, 8, 9);
[MagV2V,PhV2V] = getData(vertLPRTV, 10, 11);
[MagV2Y,PhV2Y] = getData(vertYPYPS1PS2, 2, 3);


%% Yaw Input
fY = yawLPRTV(:,1);
[MagY2L,PhY2L] = getData(yawLPRTV, 2, 3);
[MagY2P,PhY2P] = getData(yawLPRTV, 4, 5);
[MagY2R,PhY2R] = getData(yawLPRTV, 6, 7);
[MagY2T,PhY2T] = getData(yawLPRTV, 8, 9);
[MagY2V,PhY2V] = getData(yawLPRTV, 10, 11);

[MagY2Y,PhY2Y] = getData(yawYPYPS, 2, 3);
[MagY2OpP,PhY2OpP] = getData(yawYPYPS, 4, 5);
[MagY2OpY,PhY2OpY] = getData(yawYPYPS, 6, 7);
[MagY2PS,PhY2PS] = getData(yawYPYPS, 8, 9);


function [Mag, Phase] = getData(file, RealCol, ImagCol)
complex = file(:,RealCol)+ file(:,ImagCol)*1i;
Mag = squeeze(abs(complex));
Phase = squeeze(unwrap(angle(complex))*180/pi);
end