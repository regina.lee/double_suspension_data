% clear all;
%******************************************************************
ValuesC3 % input parameters from ValuesC3.m
%******************************************************************
% LONGITUDINAL AND TILT FREQUENCIES UPDATED
%******************************************************************
k11 = -m12*g*d0/I1y-2*k1*s0^2*c1^2/I1y-m2*g*d1/I1y-2*k2*s2^2*c2^2/I1y-...
m2*g*d1^2/I1y/l2/c2-m12*g*d0^2/I1y/l1/c1-m12*g*s0^2*si1^2/I1y/l1/c1-...
m2*g*s2^2*si2^2/I1y/l2/c2;
k12 = -m2*g*d1/I1y/l2/c2+m12*g*d0/I1y/l1/c1;
k13 = -m2*g*d1*d2/I1y/l2/c2+2*k2*s2^2*c2^2/I1y+...
m2*g*s2^2*si2^2/l2/c2/I1y;
k14 = +m2*g*d1/I1y/l2/c2;

k21 = +m12*g*d0/m1/l1/c1-m2*g*d1/m1/l2/c2;
k22 = -m12*g/m1/l1/c1-m2*g/m1/l2/c2;
k23 = -m2*g*d2/m1/l2/c2;
k24 = +m2*g/m1/l2/c2;

k31 = +2*k2*s2^2*c2^2/I2y-m2*g*d2*d1/I2y/l2/c2+m2*g*s2^2*si2^2/I2y/l2/c2;
k32 = -m2*g*d2/I2y/l2/c2;
k33 = -m2*g*d2/I2y-2*k2*s2^2*c2^2/I2y-...
m2*g*d2^2/I2y/l2/c2-m2*g*s2^2*si2^2/l2/I2y/c2;
k34 = +m2*g*d2/I2y/l2/c2;

k41 = +m2*g*d1/m2/l2/c2;
k42 = +m2*g/m2/l2/c2;
k43 = +m2*g*d2/m2/l2/c2;
k44 = -m2*g/m2/l2/c2;
%*************
%******************************************************************
%state space matrices

% q1 xn q2 xm Q1 vn Q2 vm
A=[0 0 0 0 1 0 0 0;
0 0 0 0 0 1 0 0;
0 0 0 0 0 0 1 0;
0 0 0 0 0 0 0 1;
k11 k12 k13 k14 -b1 0 0 0;
k21 k22 k23 k24 0 -b2 0 0;
k31 k32 k33 k34 0 0 -b3 0;
k41 k42 k43 k44 0 0 0 -b4];

X0 =-m12*g*d0/I1y/l1/c1; %x0 input for torque equation (sus pt)
PX0 = +m12*g/m1/l1/c1; %x0 input for length equation (sus pt)
Th0 = -2*k1*s1^2/I1y/c1; %theta0 input for torque equation (sus pt)

% Inputs x0, theta0, force, force input above COM
B=[
0 0 0 0 X0 PX0 0 0
0 0 0 0 Th0 0 0 0
0 0 0 0 0 1/m1 0 0
0 0 0 0 -v/I1y 1/m1 0 0
0 0 0 0 1/I1y 0 0 0]';
% 
% B=[
% 0 0 0 0 XX 0 0 0
% 0 0 0 0 0 YY 0 0]';

C=[1 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 0
    0 0 1 0 0 0 0 0
    0 0 0 1 0 0 0 0];   

D = [0];


%% VERTICAL MODES
h1 = 2*k1*c1^2+m12*g*si1^2/l1/c1;
h2 = 2*k2*c2^2+m2*g*si2^2/l2/c2;
h11 = - h1/m1 - h2 /m1;
h12 = h2/m1 ;
h21 = h2 / m2;
h22 = - h2 / m2;
%**********************************************************
% state space matrices
% x1 x2 v1 v2
Av = [ 0 0 1 0 
0 0 0 1
h11 h12 -b1 0
h21 h22 0 -b2];

%Displacement; Force
Bv = [ 0 0 h1 / m1 0
0 0 1 / m1 0]';

Cv = [ 1 0 0 0
0 1 0 0];

Dv = [0];


%% YAW FREQUENCY
AA = m12*g/l1/c1*(s0^2*c1^2+n0*n1)+2*k1*s0^2*(n1-n0)^2/l1^2;
BB = m2*g/l2/c2*(s2^2*c2^2+n2*n3)+2*k2*s2^2*(n3-n2)^2/l2^2;
i11 = -AA/I1z-BB/I1z;
i12 = BB/I1z;
i21 = BB/I2z;
i22 = -BB/I2z;
b1 = 0.5;
b2 = 0.5;
%******************************************************************
% state space matrices
% theta1 theta2 thetadot1 thetadot2
Ay = [ 0 0 1 0
0 0 0 1
i11 i12 -b1 0
i21 i22 0 -b2];
 
By = [ 0 0 1/I1z 0]';

Cy = [1 0 0 0
    0 1 0 0];

Dy = [0];


%% Trans and Roll straight wires
j11 = -m12*g*d0/I1x-2*k1*n1^2/I1x-m2*g*d1/I1x-2*k2*n3^2/I1x-...
    m2*g*d1^2/I1x/l2-m12*g*d0^2/I1x/l1;
j12 = -m2*g*d1/I1x/l2+m12*g*d0/I1x/l1;
j13 = -m2*g*d1*d2/I1x/l2+2*k2*n3^2/I1x;
j14 = +m2*g*d1/I1x/l2;

j21 = +m12*g*d0/m1/l1-m2*g*d1/m1/l2;
j22 = -m12*g/m1/l1-m2*g/m1/l2;
j23 = -m2*g*d2/m1/l2;
j24 = +m2*g/m1/l2;

j31 = +2*k2*n3^2/I2x-m2*g*d2*d1/I2x/l2;
j32 = -m2*g*d2/I2x/l2;
j33 = -m2*g*d2/I2x-2*k2*n3^2/I2x-m2*g*d2^2/I2x/l2;
j34 = +m2*g*d2/I2x/l2;

j41 = +m2*g*d1/m2/l2;
j42 = +m2*g/m2/l2;
j43 = +m2*g*d2/m2/l2;
j44 = -m2*g/m2/l2;

Y0 =-m12*g*d0/I1x/l1; %y0 input for torque equation (sus pt)
PY0 = +m12*g/m1/l1; %y0 input for length equation (sus pt)

Atr = [0 0 0 0 1 0 0 0;
    0 0 0 0 0 1 0 0;
    0 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 1;
    j11 j12 j13 j14 -b1 0 0 0;
    j21 j22 j23 j24 0 -b2 0 0;
    j31 j32 j33 j34 0 0 -b3 0;
    j41 j42 j43 j44 0 0 0 -b4];
Btr=[
0 0 0 0 Y0 PY0 0 0
0 0 0 0 1/I1x 0 0 0
0 0 0 0 d0/I1x 1/m1 0 0]';

Ctr = [1 0 0 0 0 0 0 0
    0 1 0 0 0 0 0 0
    0 0 1 0 0 0 0 0
    0 0 0 1 0 0 0 0];

Dtr = [0];
%% TRANS AND ROLL with angled wires
% j11 = -m12*g*d0/I1x+m12*g*n1*si1/I1x/c1-2*k1*n1^2/I1x2*k1*n1*d0*si1/I1x/c1 ...
%     -m2*g*d1/I1x-m2*g*n2*si2/I1x/c2- ...
%     2*k2*n2^2/I1x+2*k2*n2*d1*si2/I1x/c2+m12*g*d0/I1x*(n1*si1/l1- ...
%     d0*c1/l1)-m12*g*n1*si1/I1x/c1*(n1*si1/l1- ... 
%     d0*c1/l1)+2*k1*l1*si1*n1/I1x*(n1*si1/l1- ... 
%     d0*c1/l1)+2*k1*l1*d0*si1^2/I1x/c1*(n1*si1/l1-d0*c1/l1)+m2*g*d1/I1x*(- ... 
%     n2*si2/l2-d1*c2/l2)+m2*g*n2*si2/I1x/c2*(-n2*si2/l2-d1*c2/l2)- ...
%     2*k2*l2*si2*n2/I1x*(-n2*si2/l2-d1*c2/l2)+2*k2*l2*d1*si2^2/I1x/c2*(- ...
%     n2*si2/l2-d1*c2/l2);
% j12 = -m2*g*d1*c2/I1x/l2-m2*g*n2*si2/I1x/l2+2*k2*si2*n2*c2/I1x2*k2*d1*si2^2/I1x ... 
%     +m12*g*d0*c1/I1x/l1- m12*g*n1*si1/I1x/l1+2*k1*si1*n1*c1/I1x ... 
%     +2*k1*d0*si1^2/I1x;
% j13 = m2*g*d1/I1x*(n3*si2/l2-d2*c2/l2)+m2*g*n2*si2/I1x/c2*(n3*si2/l2-... 
%     d2*c2/l2)-2*k2*l2*si2*n2/I1x*(n3*si2/l2-d2*c2/l2)+ ... 
%     2*k2*l2*d1*si2^2/I1x/c2*(n3*si2/l2-d2*c2/l2)+2*k2*n2*n3/I1x2*k2*d1*si2*n3/I1x/c2;
% j14 = +m2*g*d1*c2/I1x/l2+m2*g*n2*si2/I1x/l2- ...
%     2*k2*si2*n2*c2/I1x+2*k2*d1*si2^2/I1x;
% 
% j21 = -m12*g/m1*(n1*si1/l1-d0*c1/l1)-2*k1*l1*si1^2/m1/c1*(n1*si1/l1- ...
%      d0*c1/l1)+2*k1*n1*si1/m1/c1+m2*g/m1*(-n2*si2/l2-d1*c2/l2) ... 
%      +2*k2*l2*si2^2/m1/c2*(-n2*si2/l2-d1*c2/l2)+2*k2*n2*si2/m1/c2;
% j22 = -m12*g*c1/m1/l1-m2*g*c2/m1/l2-2*k1*si1^2/m1-2*k2*si2^2/m1;
% j23 = -2*k2*n3*si2/m1/c2+m2*g/m1*(n3*si2/l2- ...
%     d2*c2/l2)+2*k2*l2*si2^2/m1/c2*(n3*si2/l2-d2*c2/l2);
% j24 = +m2*g*c2/m1/l2+2*k2*si2^2/m1;
% 
% j31 =+m2*g*d2/I2x*(-n2*si2/l2-d1*c2/l2)-m2*g*n3*si2/I2x/c2*(-n2*si2/l2-...
%     d1*c2/l2)+2*k2*l2*si2*n3/I2x*(-n2*si2/l2-d1*c2/l2)+...
%     2*k2*l2*d2*si2^2/I2x/c2*(-n2*si2/l2-d1*c2/l2)+...
%     2*k2*n2*n3/I2x+2*k2*d2*n2*si2/I2x/c2;
% j32 = -(+m2*g*d2*c2/I2x/l2-m2*g*n3*si2/I2x/l2 ... 
%     +2*k2*si2*n3*c2/I2x+2*k2*d2*si2^2/I2x);
% j33 = +m2*g*d2/I2x*(n3*si2/l2-d2*c2/l2)-m2*g*n3*si2/I2x/c2*(n3*si2/l2-...
%     d2*c2/l2)+2*k2*l2*si2*n3/I2x*(n3*si2/l2-d2*c2/l2)+...
%     2*k2*l2*d2*si2^2/I2x/c2*(n3*si2/l2-d2*c2/l2)-2*k2*n3*n3/I2x- ...
%     2*k2*d2*n3*si2/I2x/c2-m2*g*d2/I2x+m2*g*n3*si2/I2x/c2-...
%     2*k3*l3*si3*n4/I2x*(-n4*si3/l3-d3*c3/l3)+2*k3*l3*d3*si3^2/I3x/c3*...
%     (-n4*si3/l3-d3*c3/l3)-2*k3*n4*n4/I2x+2*k3*d3*n4*si3/I2x/c3;
% j34 =-(-2*k3*si3*n4*c3/I2x+2*k3*d3*si3^2/I2x)+m2*g*d2*c2/I2x/l2- ...
%     m2*g*n3*si2/I2x/l2+2*k2*si2*n3*c2/I2x+2*k2*d2*si2^2/I2x;
% 
% j41 = +m2*g*si2*n2/m2/l2+m2*c2*g*d1/m2/l2+2*k2*si2^3*n2/m2/c2+...
%     2*k2*si2^2*d1/m2-2*k2*n2*si2/m2/c2;
% j42 = +m2*g*c2/m2/l2+2*k2*si2^2/m2;
% j43 = -m2*g*n3*si2/m2/l2+m2*g*d2*c2/m2/l2-2*k2*l2*si2^2/m2/c2*...
%     (n3*si2/l2-d2*c2/l2)+2*k2*n3*si2/m2/c2+2*k3*l3*si3^2/m2/c3*(-n4*si3/l3-...
%     d3*c3/l3)+2*k3*n4*si3/m2/c3;
% j44 = -m2*g*c2/m2/l2-2*k2*si2^2/m2-2*k3*si3^2/m2;


