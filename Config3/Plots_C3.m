clear all;close all;
DataLoadConfig3;
config = '3';
path = 'C:\Users\dell\Documents\ligo-suspension-thesis-materials\Thesis\Figures\ExperimentalResults\';
%% Longitudinal Force to Length Top Mass
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(fL,MagL2L*1/ct2N,'-o','MarkerSize',2,'LineWidth',3); %L to L Experimental
hold on
loglog(wModelInFL/2/pi,squeeze(MagModelInFL),'LineWidth',3); % L to L Model
xlim([0.1,60]);
title('Configuration 3 - Comparison of Top Mass State Space Model and Experimental Data');
ylabel('Magnitude (m/N)')
grid on;
set(findall(gcf,'-property','FontSize'),'FontSize',20)

subplot(2,1,2);
semilogx(fL,PhL2L,'-o','MarkerSize',2,'LineWidth',3); % L to L Experimental
hold on;
semilogx(wModelInFL/2/pi,squeeze(PhModelInFL),'LineWidth',3); % L to L Model
grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('Longitudinal-to-Longitudinal Experimental','Longitudinal-to-Longitudinal Model');
legend('Location','southwest')
in = 'L';
out = 'L';
saveas(gcf,strcat(path,'Config',config,in,'2',out,'.png'))
%% Longitudinal Force to Pitch Top Mass
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(fL, MagL2P*1/ct2N,'-o','LineWidth',3,'MarkerSize',2); %L to P Experimental
hold on
loglog(wModelInF/2/pi,squeeze(MagModelInF),'LineWidth',3); % L to P Model
xlim([0.1,60]);
title('Configuration 3 - Comparison of Top Mass State Space Model and Experimental Data');
ylabel('Magnitude (\phi/N)')
grid on;
set(findall(gcf,'-property','FontSize'),'FontSize',20)

subplot(2,1,2);
semilogx(fL,PhL2P,'-o','LineWidth',3,'MarkerSize',2); % L to P Experimental 
hold on;
semilogx(wModelInF/2/pi,squeeze(PhModelInF),'LineWidth',3); % L to P Model
grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('Longitudinal-to-Pitch Experimental','Longitudinal-to-Pitch Model');
legend('Location','southwest')
in = 'L';
out = 'P';
saveas(gcf,strcat(path,'Config',config,in,'2',out,'.png'))

%% Longitudinal Force to Length Bottom Mass
figure('units','normalized','outerposition',[0 0 1 1])

subplot(2,1,1);
loglog(fL2,MagL2PS1*.006/.046,'-o','MarkerSize',2,'LineWidth',3); %L to L Experimental
hold on
loglog(wModelLLB/2/pi,squeeze(MagModelLLB),'LineWidth',3); % L to L Model Bottom Mass
xlim([0.1,60]);
title('Configuration 3 - Comparison of Bottom Mass State Space Model and Experimental Data');
ylabel('Magnitude (m/N)')
grid on;
set(findall(gcf,'-property','FontSize'),'FontSize',20)

subplot(2,1,2);
semilogx(fL2,PhL2PS1,'-o','MarkerSize',2,'LineWidth',3); % L to L Experimental
hold on;
semilogx(wModelLLB/2/pi,squeeze(PhModelLLB),'LineWidth',3); % L to L Model Bottom Mass
grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('Longitudinal-to-Longitudinal Experimental','Longitudinal-to-Longitudinal Model');
legend('Location','southwest')
in = 'L';
out = 'LB';
saveas(gcf,strcat(path,'Config',config,in,'2',out,'.png'))

%% Longitudinal Force to Pitch Bottom Mass
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(fL2,MagL2OpP*.014/2.82,'-o','MarkerSize',2,'LineWidth',3); %L to L Experimental
hold on
loglog(wModelLPB/2/pi,squeeze(MagModelLPB),'LineWidth',3); % L to L Model Bottom Mass
xlim([0.1,60]);
title('Configuration 3 - Comparison of Bottom Mass State Space Model and Experimental Data');
ylabel('Magnitude (\phi/N)')
grid on;
set(findall(gcf,'-property','FontSize'),'FontSize',20)

subplot(2,1,2);
semilogx(fL2,PhL2OpP,'-o','MarkerSize',2,'LineWidth',3); % L to L Experimental
hold on;
semilogx(wModelLPB/2/pi,squeeze(PhModelLPB),'LineWidth',3); % L to L Model Bottom Mass
grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('Longitudinal-to-Pitch Experimental','Longitudinal-to-Pitch Model');
legend('Location','southwest')
in = 'L';
out = 'PB';
saveas(gcf,strcat(path,'Config',config,in,'2',out,'.png'))

%% All length input
figure;
subplot(2,1,1);
loglog(fL,MagL2L);
hold on
loglog(fL,MagL2P);
loglog(fL,MagL2R);
loglog(fL,MagL2T);
loglog(fL,MagL2V);
loglog(fL,MagL2Y);
title('Input Longitudinal Force to all Top Mass DOFs')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,60]);
grid on;
%   

subplot(2,1,2);
semilogx(fL,PhL2L);
hold on;
semilogx(fL,PhL2P);
semilogx(fL,PhL2R);
semilogx(fL,PhL2T);
semilogx(fL,PhL2V);
semilogx(fL,PhL2Y);
ylim([-1000,200]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
xlim([0.1,70])
grid on;
% legend('L-to-L','L-to-P');
legend('L-to-L','L-to-P','L-to-R','L-to-T','L-to-V','L-to-Y');
%% Pitch Input
figure;
subplot(2,1,1);

loglog(fP,MagP2P,'MarkerSize',2);
hold on
loglog(fP,MagP2L,'-.');
% loglog(wout/2/pi,MagSim,'-.');
xlim([0.1,30]);
  

title('Input Pitch to Top Mass')
ylabel('Mag [\mum/cts, \murad/cts]')

grid on;
subplot(2,1,2);
semilogx(fP,PhP2P);
hold on;
semilogx(fP,PhP2L);
% semilogx(wout/2/pi,PhSim,'-.');
legend('Pitch/Pitch','Length/Pitch','Simulated P/P');
  
grid on;
ylim([-400,300]);
xlim([0.1,30]);

ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
%% All Pitch Input
figure;
subplot(2,1,1);
loglog(fP,MagP2L);
hold on
loglog(fP,MagP2P);
loglog(fP,MagP2R);
loglog(fP,MagP2T);
loglog(fP,MagP2V);
loglog(fP,MagP2Y);
title('Input Pitch to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,30]);
grid on;
  

subplot(2,1,2);
semilogx(fP,PhP2L);
hold on;
semilogx(fP,PhP2P);
semilogx(fP,PhP2R);
semilogx(fP,PhP2T);
semilogx(fP,PhP2V);
semilogx(fP,PhP2Y);
ylim([-800,400]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
xlim([0.1,30]);
grid on;
legend('P-to-L','P-to-P','P-to-R','P-to-T','P-to-V','P-to-Y');
  ;
%% Roll Input
figure;
subplot(2,1,1);
loglog(fR,MagR2L);
hold on
loglog(fR,MagR2P);
loglog(fR,MagR2R);
loglog(fR,MagR2T);
loglog(fR,MagR2V);
loglog(fR,MagR2Y);
title('Input Roll to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,20]);
grid on;
  

subplot(2,1,2);
semilogx(fR,PhR2L);
hold on;
semilogx(fR,PhR2P);
semilogx(fR,PhR2R);
semilogx(fR,PhR2T);
semilogx(fR,PhR2V);
semilogx(fR,PhR2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('R-to-L','R-to-P','R-to-R','R-to-T','R-to-V','R-to-Y');
  
%% T-to-T and T-to-R
figure;
subplot(2,1,1);
loglog(fT,MagT2R, 'LineWidth',3);
hold on;
loglog(fT,MagT2T, 'LineWidth',3);
title('Input Transverse to Top Mass - Config 3')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,70]);
grid on;


subplot(2,1,2);
semilogx(fT,PhT2R, 'LineWidth',3);
hold on;
semilogx(fT,PhT2T, 'LineWidth',3);
ylim([-800,400]);
xlim([0.1,70]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('T-to-R','T-to-T');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
%% Trans Input
figure;
subplot(2,1,1);
loglog(fT,MagT2L);
hold on
loglog(fT,MagT2P);
loglog(fT,MagT2R);
loglog(fT,MagT2T);
loglog(fT,MagT2V);
loglog(fT,MagT2Y);
title('Input Transverse to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,20]);
grid on;
  

subplot(2,1,2);
semilogx(fT,PhT2L);
hold on;
semilogx(fT,PhT2P);
semilogx(fT,PhT2R);
semilogx(fT,PhT2T);
semilogx(fT,PhT2V);
semilogx(fT,PhT2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('T-to-L','T-to-P','T-to-R','T-to-T','T-to-V','T-to-Y');
  ;

%% Vert Input
figure;
subplot(2,1,1);
loglog(fV,MagV2L);
hold on
loglog(fV,MagV2P);
loglog(fV,MagV2R);
loglog(fV,MagV2T);
loglog(fV,MagV2V);
loglog(fV,MagV2Y);
title('Input Vertical to Top Mass')
xlim([0.1,20]);
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
  

subplot(2,1,2);
semilogx(fV,PhV2L);
hold on;
semilogx(fV,PhV2P);
semilogx(fV,PhV2R);
semilogx(fV,PhV2T);
semilogx(fV,PhV2V);
semilogx(fV,PhV2Y);
ylim([-1000,200]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('V-to-L','V-to-P','V-to-R','V-to-T','V-to-V','V-to-Y');
  ;

%% Yaw Input
figure;
subplot(2,1,1);
loglog(fY,MagY2L);
hold on
loglog(fY,MagY2P);
loglog(fY,MagY2R);
loglog(fY,MagY2T);
loglog(fY,MagY2V);
loglog(fY,MagY2Y);
xlim([0.1,20]);
title('Input Yaw to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
  

subplot(2,1,2);
semilogx(fY,PhY2L);
hold on;
semilogx(fY,PhY2P);
semilogx(fY,PhY2R);
semilogx(fY,PhY2T);
semilogx(fY,PhY2V);
semilogx(fY,PhY2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('Y-to-L','Y-to-P','Y-to-R','Y-to-T','Y-to-V','Y-to-Y');
  ;

%% Diagonal TFs
figure('units','normalized','outerposition',[0 0 1 1]);
subplot(2,1,1);
loglog(fL,MagL2L);
hold on;
loglog(fP, MagP2P,'-o','MarkerSize',2);
loglog(fR, MagR2R,'-o','MarkerSize',2);
loglog(fT, MagT2T,'-o','MarkerSize',2);
loglog(fY, MagY2Y,'-o','MarkerSize',2);
loglog(fV, MagV2V,'-o','MarkerSize',2);
loglog(fL, MagL2P,'-o','MarkerSize',2);
xlim([0.1,60]);
title('Diagonal Transfer Functions - Config 3');
ylabel('Magnitude')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-3 10^-1 10^1 10^3 ])
set(findall(gcf,'type','line'),'linewidth',3)
grid on;

subplot(2,1,2);
semilogx(fL, PhL2L,'-o','MarkerSize',2);
hold on
semilogx(fP, PhP2P,'-o','MarkerSize',2);
semilogx(fR, PhR2R,'-o','MarkerSize',2);
semilogx(fT, PhT2T,'-o','MarkerSize',2);
semilogx(fY, PhY2Y,'-o','MarkerSize',2);
semilogx(fV, PhV2V,'-o','MarkerSize',2);
grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
legend( 'L-to-L','P-to-P','R-to-R','T-to-T','Y-to-Y','V-to-V');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(findall(gcf,'type','line'),'linewidth',3)
legend('Location','west')
saveas(gcf,strcat(path,'C',config,'DiagonalTFs.png'))