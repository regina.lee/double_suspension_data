pitch_radius = 58; %58 mm
% 1 rad = 190,000 cts
% 1rad/58mm * 1mm/2V * 40V/2^16 cts
ct2rad = 1/pitch_radius*1/2*40/2^16;
ct = 5000;
rad = ct*ct2rad;
deg = rad*180/pi;
vert = sin(rad)*90; %mm
