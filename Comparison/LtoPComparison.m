%% Load Data
clear all; close all;
lengthC1 = load(fullfile('../Config1/Feb14Data/LengthInputLPRTV'));
lengthC2 = load(fullfile('../Config2/DataConfig2/LongDriveLPRTV'));
lengthC3 = load(fullfile('../Config3/DataConfig3/LongDriveLPRTV'));
lengthC4 = load(fullfile('../Config4/DataConfig4/522/LongDriveLPRTV'));

pitchC1 = load(fullfile('../Config1/Feb14Data/PitchInputLPRTV'));
pitchC2 = load(fullfile('../Config2/DataConfig2/PitchDriveLPRTV'));
pitchC3 = load(fullfile('../Config3/DataConfig3/PitchDriveLPRTV'));
pitchC4 = load(fullfile('../Config4/DataConfig4/522/PitchDriveLPRTV'));

fLC1 = lengthC1(:,1);
[MagL2LC1,PhL2LC1] = getData(lengthC1, 2, 3);
[MagL2PC1,PhL2PC1] = getData(lengthC1, 4, 5);

fLC2 = lengthC2(:,1);
[MagL2LC2,PhL2LC2] = getData(lengthC2, 2, 3);
[MagL2PC2,PhL2PC2] = getData(lengthC2, 4, 5);

fLC3 = lengthC3(:,1);
[MagL2LC3,PhL2LC3] = getData(lengthC3, 2, 3);
[MagL2PC3,PhL2PC3] = getData(lengthC3, 4, 5);

fLC4 = lengthC4(:,1);
[MagL2LC4,PhL2LC4] = getData(lengthC4, 2, 3);
[MagL2PC4,PhL2PC4] = getData(lengthC4, 4, 5);

fPC1 = pitchC1(:,1);
[MagP2LC1, PhP2LC1] = getData(pitchC1, 2, 3);
[MagP2PC1, PhP2PC1] = getData(pitchC1, 4, 5);

fPC2 = pitchC2(:,1);
[MagP2LC2, PhP2LC2] = getData(pitchC2, 2, 3);
[MagP2PC2, PhP2PC2] = getData(pitchC2, 4, 5);

fPC3 = pitchC3(:,1);
[MagP2LC3, PhP2LC3] = getData(pitchC3, 2, 3);
[MagP2PC3, PhP2PC3] = getData(pitchC3, 4, 5);

fPC4 = pitchC4(:,1);
[MagP2LC4, PhP2LC4] = getData(pitchC4, 2, 3);
[MagP2PC4, PhP2PC4] = getData(pitchC4, 4, 5);


%% Plot L-to-P
c1 = 0.0895;
c2 = 0.0952;
c3 = 0.0089;
c4 = 0.0062;
subplot(2,1,1);
loglog(fLC1,MagL2PC1*c1,'-o','LineWidth',2,'MarkerSize',2); 
hold on
loglog(fLC2,MagL2PC2*c2,'-o','LineWidth',2,'MarkerSize',2); 
loglog(fLC4,MagL2PC4*c4,'-o','LineWidth',2,'MarkerSize',2);
loglog(fLC3,MagL2PC3*c3,'-o','LineWidth',2,'MarkerSize',2); 
 
xlim([0.1,60]);
title('Comparison of 4 Configurations L-to-P Coupling');
ylabel('Magnitude (Disp (\phi)/Force (N))')
grid on;
% set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
set(findall(gcf,'-property','FontSize'),'FontSize',20)


subplot(2,1,2);
semilogx(fLC1,PhL2PC1,'-o','LineWidth',3,'MarkerSize',2); 
hold on
semilogx(fLC2,PhL2PC2,'-o','LineWidth',3,'MarkerSize',2);
semilogx(fLC4,PhL2PC4,'-o','LineWidth',3,'MarkerSize',2); 
semilogx(fLC3,PhL2PC3,'-o','LineWidth',3,'MarkerSize',2); 

grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('L-to-P d = 2cm','L-to-P d = 2mm','L-to-P d = 0','L-to-P 2 Wire d = 2cm');

%% Plot P-to-P
c1 = 0.0895;
c2 = 0.0952;
c3 = 0.0089;
c4 = 0.0062;
figure()
subplot(2,1,1);
loglog(fPC1,MagP2PC1*c1,'-o','LineWidth',2,'MarkerSize',2); 
hold on
loglog(fPC2,MagP2PC2*c2,'-o','LineWidth',2,'MarkerSize',2); 
loglog(fPC4,MagP2PC4*c4,'-o','LineWidth',2,'MarkerSize',2);
loglog(fPC3,MagP2PC3*c3,'-o','LineWidth',2,'MarkerSize',2); 
 
xlim([0.1,60]);
title('Comparison of 4 Configurations P-to-P Coupling');
ylabel('Magnitude (Disp (\phi)/Force (N))')
grid on;
% set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
set(findall(gcf,'-property','FontSize'),'FontSize',20)


subplot(2,1,2);
semilogx(fPC1,PhP2PC1,'-o','LineWidth',3,'MarkerSize',2); 
hold on
semilogx(fPC2,PhP2PC2,'-o','LineWidth',3,'MarkerSize',2);
semilogx(fPC4,PhP2PC4,'-o','LineWidth',3,'MarkerSize',2); 
semilogx(fPC3,PhP2PC3,'-o','LineWidth',3,'MarkerSize',2); 

grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
legend('P-to-P d = 2cm','P-to-P d = 2mm','P-to-P d = 0','P-to-P 2 Wire d = 2cm');


function [Mag, Phase] = getData(file, RealCol, ImagCol)
complex = file(:,RealCol)+ file(:,ImagCol)*1i;
Mag = squeeze(abs(complex));
Phase = squeeze(unwrap(angle(complex))*180/pi);
end