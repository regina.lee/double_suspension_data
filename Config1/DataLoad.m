%% Load Data
% clear all; close all;
addpath('../Simulation Data');
% addpath('../vfit3');

rollTR = load(fullfile('ExportedData/Feb8RollInputTR'));
rollVPLY = load(fullfile('ExportedData/Feb8RollInputVPLY'));
transTR = load(fullfile('ExportedData/Feb8TransInputTR'));
transVPLY = load(fullfile('ExportedData/Feb8TransInputVPLY'));
lengthLPRTV = load(fullfile('Feb14Data/LengthInputLPRTV'));
lengthYPYPS = load(fullfile('Feb14Data/LengthInputYOpPOpYPS'));
pitchLPRTV = load(fullfile('Feb14Data/PitchInputLPRTV'));
pitchYPYPS = load(fullfile('Feb14Data/PitchInputYOpPOpYPS'));
vertVRP = load(fullfile('ExportedData/Feb8VertInputVRP'));
vertLYT = load(fullfile('ExportedData/Feb8VertInputLYT'));
yawLPRTV = load(fullfile('Feb14Data/YawInputLPRTV')); 
yawYPYPS = load(fullfile('Feb14Data/YawInputYOpPOpYPS'));
groundOpPOpYPS = load(fullfile('Feb14Data/GroundInputFeb3'));
groundLPY = load(fullfile('Feb14Data/GroundInput2'));
coherenceLLLP = load(fullfile('ExportedData/Coherence_LL_LP'));
sim = load('space_states.mat');
%% Model

double_pendulum_c1;
sys = ss(A,B,C,D);
ct2N = 2^16/40*2*5.66;
% ct2N = 40/2^16/7.46*1.6;
m2ct = 1/(2^16/40*2);
% m2ct = 1/(3.112*10^3*2);
[MagModelLL, PhModelLL, wModelLL] = bode(sys(2,1),{0.1,10000}); %In length, out length top mass
[MagModelLP, PhModelLP, wModelLP] = bode(sys(1,1),{0.1,10000}); %In length, out pitch top mass
[MagModelLLB, PhModelLLB, wModelLLB] = bode(sys(4,3),{0.1,10000}); %In length, out length bottom mass
[MagModelLPB, PhModelLPB, wModelLPB] = bode(sys(3,3),{0.1,10000}); %In length, out pitch bottom mass
[MagModelInF, PhModelInF, wModelInF] = bode(sys(1,3),{0.1,10000}); %Input force, output pitch top mass
[MagModelInFL, PhModelInFL, wModelInFL] = bode(sys(2,3),{0.1,10000}); % Input force, output length top mass
[MagModelInT, PhModelInT, wModelInT] = bode(sys(1,4),{0.1,10000}); %Input torque, output pitch top mass
%% Pitch Input
fP = pitchLPRTV(:,1);

[MagP2L, PhP2L] = getData(pitchLPRTV, 2, 3);
[MagP2P, PhP2P] = getData(pitchLPRTV, 4, 5);
[MagP2R, PhP2R] = getData(pitchLPRTV, 6, 7);
[MagP2T, PhP2T] = getData(pitchLPRTV, 8, 9);
[MagP2V, PhP2V] = getData(pitchLPRTV, 10, 11);
[MagP2Y, PhP2Y] = getData(pitchYPYPS, 2, 3);

[MagP2OpP, PhP2OpP] = getData(pitchYPYPS, 4, 5);
[MagP2OpY, PhP2OpY] = getData(pitchYPYPS, 6, 7);
[MagP2PS, PhP2PS] = getData(pitchYPYPS, 8, 9);


[MagSim,PhSim,wout] = bode(sim.G(12,6));
MagSim = squeeze(MagSim);
PhSim = squeeze(PhSim);





%% Length Input
fL = lengthLPRTV(:,1);
[MagL2L,PhL2L] = getData(lengthLPRTV, 2, 3);
[MagL2P,PhL2P] = getData(lengthLPRTV, 4, 5);
[MagL2R,PhL2R] = getData(lengthLPRTV, 6, 7);
[MagL2T,PhL2T] = getData(lengthLPRTV, 8, 9);
[MagL2V,PhL2V] = getData(lengthLPRTV, 10, 11);

fL2 = lengthYPYPS(:,1);
[MagL2Y,PhL2Y] = getData(lengthYPYPS, 2, 3);
[MagL2OpP,PhL2OpP] = getData(lengthYPYPS, 4, 5);
[MagL2OpY,PhL2OpY] = getData(lengthYPYPS, 6, 7);
[MagL2PS,PhL2PS] = getData(lengthYPYPS, 8, 9);

[MagSimL,PhSimL,woutL] = bode(sim.G(6,1));
MagSimL = squeeze(MagSimL);
PhSimL = squeeze(PhSimL);
MagModelLL = squeeze(MagModelLL);
PhModelLL = squeeze(PhModelLL);
MagModelLP = squeeze(MagModelLP);
PhModelLP = squeeze(PhModelLP);

fCoherence = coherenceLLLP(:,1);
CoherenceLL = coherenceLLLP(:,2);
CoherenceLP = coherenceLLLP(:,3);

%% My attempt at fitting L-to-L
s = tf('s');
wz = 0.9063*2*pi;
zz = .00635;
wn1 = 0.6719*2*pi;
% z1 = 0.0466;
z1 = 0.016;
wn2 = 1.625*2*pi;
% z2 = 0.02584;
z2 = 0.007;

zero = s^2+2*wz*zz*s+wz^2;
p1 = s^2+2*z1*wn1*s+wn1^2;
p2 = s^2+2*z2*wn2*s+wn2^2;
k = 0.06464/0.01776;

H = k*zero/(p1*p2);


[magfitLL, phfitLL, wfitLL] = bode(-H,{0.1*2*pi,10*2*pi});


%%
% [SER,poles,rmserr,fit,opts]=vectfit3(MagL2L,fL',[0.6719*2*pi 1.625*2*pi],1);
%% Fitting L-to-P
s = tf('s');
wzP = 1.109*2*pi;
zzP = .00635;
wn1P = 0.6719*2*pi;
z1P = 0.016;
wn2P = 1.625*2*pi;
z2P = 0.01;
wn3P = 2.5*2*pi;
z3P = 0.06;

zero = s^2+2*wzP*zzP*s+wzP^2;
p1 = s^2+2*z1P*wn1P*s+wn1P^2;
p2 = s^2+2*z2P*wn2P*s+wn2P^2;
p3 = s^2+2*z3P*wn3P*s+wn3P^2;
k = 0.08291/0.000108;

LP = k*zero/(p1*p2*p3);
[magfitP,phfitP,wfitP] = bode(LP);





%% Bottom Mass TFs Length Input

MagModelLLB = squeeze(MagModelLLB);
PhModelLLB = squeeze(PhModelLLB);
MagModelLPB = squeeze(MagModelLPB);
PhModelLPB = squeeze(PhModelLPB);
MagModelInF = squeeze(MagModelInF);
PhModelInF = squeeze(PhModelInF);
MagModelInFL = squeeze(MagModelInFL);
PhModelInFL = squeeze(PhModelInFL);



%% Roll Input
fR = rollTR(:,1);
[MagR2T,PhR2T] = getData(rollTR, 2, 3);
[MagR2R,PhR2R] = getData(rollTR, 4, 5);
[MagR2V,PhR2V] = getData(rollVPLY, 2, 3);
[MagR2P,PhR2P] = getData(rollVPLY, 4, 5);
[MagR2L,PhR2L] = getData(rollVPLY, 6, 7);
[MagR2Y,PhR2Y] = getData(rollVPLY, 8, 9);





%% Trans Input
fT = transTR(:,1);
[MagT2T,PhT2T] = getData(transTR, 2, 3);
[MagT2R,PhT2R] = getData(transTR, 4, 5);
[MagT2V,PhT2V] = getData(transVPLY, 2, 3);
[MagT2P,PhT2P] = getData(transVPLY, 4, 5);
[MagT2L,PhT2L] = getData(transVPLY, 6, 7);
[MagT2Y,PhT2Y] = getData(transVPLY, 8, 9);






%% Vert Input
fV = vertVRP(:,1);
[MagV2V,PhV2V] = getData(vertVRP, 2, 3);
[MagV2R,PhV2R] = getData(vertVRP, 4, 5);
[MagV2P,PhV2P] = getData(vertVRP, 6, 7);
[MagV2L,PhV2L] = getData(vertLYT, 2, 3);
[MagV2Y,PhV2Y] = getData(vertLYT, 4, 5);
[MagV2T,PhV2T] = getData(vertLYT, 6, 7);





%% Yaw Input
fY = yawLPRTV(:,1);
[MagY2L,PhY2L] = getData(yawYPYPS, 2, 3);
[MagY2P,PhY2P] = getData(yawLPRTV, 4, 5);
[MagY2R,PhY2R] = getData(yawLPRTV, 6, 7);
[MagY2T,PhY2T] = getData(yawYPYPS, 8, 9);
[MagY2V,PhY2V] = getData(yawLPRTV, 10, 11);

[MagY2Y,PhY2Y] = getData(yawYPYPS, 2, 3);
[MagY2OpP,PhY2OpP] = getData(yawYPYPS, 4, 5);
[MagY2OpY,PhY2OpY] = getData(yawYPYPS, 6, 7);
[MagY2PS,PhY2PS] = getData(yawYPYPS, 8, 9);



%% Ground Input
fG = groundOpPOpYPS(:,1);
fG2 = groundLPY(:,1);
[MagG2OpP, PhG2OpP] = getData(groundOpPOpYPS, 2, 3);
[MagG2OpY, PhG2OpY] = getData(groundOpPOpYPS, 4, 5);
[MagG2PS, PhG2PS] = getData(groundOpPOpYPS, 6, 7);
[MagG2L, PhG2L] = getData(groundLPY, 2, 3);
[MagG2P, PhG2P] = getData(groundLPY, 4, 5);
[MagG2Y, PhG2Y] = getData(groundLPY, 6, 7);


%% Full Matrix of TF
% H = [V2V V2R V2P V2L V2Y V2T;
%     R2V R2R R2P R2L R2Y R2T;
%     P2V P2R P2P P2L P2Y P2T;
%     L2V L2R L2P L2L L2Y L2T;
%     Y2V Y2R Y2P Y2L Y2Y Y2T;
%     T2V T2R T2P T2L T2Y T2T];
% 
% Eul2Os = [1 0.065 0 0 0 0;
%     1 -0.065 -0.058 0 0 0;
%     1 -0.065 -0.058 0 0 0;
%     0 0 0 1 -0.06 0;
%     0 0 0 1 0.06 0;
%     0 0 0 0 0 1];
% 
% Os2Eul = [0.5 0.25 0.25 0 0 0;
%     7.692 -3.846 -3.846 0 0 0;
%     0 8.62 -8.62 0 0 0;
%     0 0 0 0.5 -8.33 0;
%     0 0 0 0.5 8.33 0;
%     0 0 0 0 0 1];

% G = Os2Eul*H*Eul2Os;
function [Mag, Phase] = getData(file, RealCol, ImagCol)
complex = file(:,RealCol)+ file(:,ImagCol)*1i;
Mag = squeeze(abs(complex));
Phase = squeeze(unwrap(angle(complex)))*180/pi;
end