g =9.81;
%******************************************************************
ux = 0.196; % dimensions of UPPER MASS (square) in m
uy = 0.18;
uz = 0.05;
den1 = 2700; % density (aluminium)
% m1 = den1*uy*uz*ux; % mass
m1 = 2.880;
I1x = m1*(uy^2+uz^2)/12; % moment of inertia (sideways roll)
I1y = m1*(uz^2+ux^2)/12; % moment of inertia (longitudinal tilt)
I1z = m1*(uy^2+ux^2)/12; % moment of inertia (rotation)
%******************************************************************
ix = 0.075; % dimension of TEST MASS (cylinder)
ir = 0.075;
den2 = 2700; % density (aluminum)
% m2 = den2*pi*ir^2*ix; % intermediate mass
m2 = 2.662;
I2x = m2*(ir^2/2); % moment of inertia (sideways roll)
I2y = m2*(ir^2/4+ix^2/12); % moment of inertia (longitudinal tilt)
I2z = m2*(ir^2/4+ix^2/12); % moment of inertia (rotation)
%******************************************************************
l1 = 0.3; % upper wire length
l2 = 0.3; % intermediate wire length
%******************************************************************
nw1 = 4; % number of wires per stage (2 or 4)
nw2 = 4; % = number of cantilevers (if fitted)
%******************************************************************
r1 = 0.1e-3; % radius of upper wire
r2 = 0.1e-3; % radius of intermediate wire
%******************************************************************
Y1 = 1.65e11; % Youngs Modulus of upper wire (s/steel 302)
Y2 = 1.65e11; % Youngs Modulus of intermediate wire (s/steel 302)
%******************************************************************
% uncoupled mode frequency of cantilever stage(=0 for no cantilevers)
ufc1 = 1.8;
ufc2 = 0;
ufc3 = 0;
% NB:- uncoupled mode frequency- the frequency observed for a cantilever in a
% particular stage supporting only the mass of that stage
%******************************************************************
d0 = 0.0055/2; % height of upper wire break-off (above c.of m. upper mass)
d1 = 0.015; % height of intermediate wire break-off (below c.of m. upper mass)
d2 = 0.001; % height of intermediate wire break-off (above c.of m. of int. mass)

v = 0.006; % Distance forcing above COM

%******************************************************************
% X direction separation
su = 0.12/2; % 1/2 separation of upper wires
si = 0.04/2; % 1/2 separation of intermediate wires
%******************************************************************
% Y direction separation
n0 = 0.194/2; % 1/2 separation of upper wires at suspension point
n1 = 0.194/2; % 1/2 separation of upper wires at upper mass
n2 = 0.194/2; % 1/2 separation of intermediate wires at upper mass
n3 = .155/2; % 1/2 separation of int. wires at intermediate mass
%******************************************************************
% represents small loss (so as not dividing by zero)
% b1 = 0.01;b2=0.01;b3=0.01;b4=0.01;
b1 = 0.03;b2=0.03;b3=0.03;b4=0.03;
%******************************************************************
% END OF INPUT
%******************************************************************
% CALCULATIONS
%***************************************************************
% spring constants
% kc1 = 1/2 * (2*pi*ufc1)^2*m1;
E = 186*10^9;
a = 18*10^-3;
h = 0.76*10^-3;
lb = 120*10^-3;
% alpha = 10/12;
% alpha = 10/18;

% kc1 = E*a*h^3/4/lb^3/alpha;
% kc1 = 208.5254;
% kc1 = 262;
kc1 = 274.5984;
kc2 = 1/2 * (2*pi*ufc2)^2*m2;
kw1 = Y1*pi*r1^2/l1*nw1/2;
kw2 = Y2*pi*r2^2/l2*nw2/2;
if (kc1 == 0)
k1 = kw1;
else
k1 = kc1*kw1/(kc1+kw1);
end
if (kc2 == 0)
k2 = kw2;
else
k2 = kc2*kw2/(kc2+kw2);
end
%******************************************************************
% in X-direction wires must be vertical
s0 = su;s1 = su;
s2 = si;s3 = si;
%******************************************************************

m12 = m1+m2;
%******************************************************************
% cosine and sine of the angle the wire makes with the vertical (z)
si1 = (s1-s0)/l1; % sin(Ω1)
si2 = (s3-s2)/l2; % sin(Ω2)
c1 = (l1^2-(s1-s0)^2)^0.5/l1; % cos(Ω1)
c2 = (l2^2-(s3-s2)^2)^0.5/l2; % cos(Ω2)

siy1 = (n1-n0)/l1; % sin(Ω1)
siy2 = (n3-n2)/l2; % sin(Ω2)
c1y = (l1^2-(n1-n0)^2)^0.5/l1; % cos(Ω1)
c2y = (l2^2-(n3-n2)^2)^0.5/l2; % cos(Ω2)
%******************************************************************
