clear all; close all;
DataLoadConfig2;

%% Plot Length Input

figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(fL,MagL2L,'-.','LineWidth',3); %L to P Experimental
hold on
loglog(wModelInFL/2/pi,squeeze(MagModelInFL),'LineWidth',3); % L to P Model
% loglog(wModelInF/2/pi,squeeze(MagModelInF),'LineWidth',3); % L to P Model
% loglog(wModelTR/2/pi,squeeze(10^5*MagModelTR),'LineWidth',3); % Combined Yaw and Length
% loglog(fL,MagL2L,'LineWidth',3);
% loglog(wout/2/pi,MagSim,'LineWidth',3);
% title('Input Length to Top Mass')
title('Comparison of State Space Model and Experimental Data Top Mass - L-to-P');
% ylabel('Magnitude (\mum/counts, \murad/counts)')
ylabel('Magnitude (\phi/F)')
xlim([0,100]);
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
grid on;
% plot_darkmode

% 
subplot(2,1,2);
semilogx(fL,PhL2L,'-.','LineWidth',3); % L to P Experimental 
hold on;
semilogx(fL,PhL2L,'LineWidth',3);
% semilogx(wModelInT/2/pi,squeeze(PhModelInT),'LineWidth',3); % L to L Model
% % semilogx(wModelYT/2/pi,squeeze(PhModelYT),'LineWidth',3); % L to L Model
% grid on;
% ylim([-800,400]);
% % xlim([0.1,10]);
% ylabel('Phase (deg)')
% xlabel('Frequency (Hz)')
% % legend('Length/Length','Pitch/Length');
% legend('L-P Experimental','L-P State Space Model');
% set(findall(gcf,'-property','FontSize'),'FontSize',20)
% % plot_darkmode
%% Plot Damped L-to-L
figure;
subplot(2,1,1);
loglog(fD,squeeze(MagOL),'-.','LineWidth',3); %OL Experimental
hold on
loglog(fD,squeeze(MagCL),'LineWidth',3); %CL Experimental
xlim([0.01,80]);
title('Damping of Top Mass (Force to Displacement)');
% ylabel('Magnitude (\mum/counts, \murad/counts)')
ylabel('Magnitude (\mum/counts)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-3 10^-2 10^-1 10^0 10^1])
ylim([1e-3,1e1]);
grid on;
% plot_darkmode


subplot(2,1,2);
semilogx(fD,squeeze(PhOL),'-.','LineWidth',3); % OL Experimental
hold on;
semilogx(fD,squeeze(PhCL),'LineWidth',3); % CL Experimental 
ylim([-800,400]);
xlim([0.01,80]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
% legend('Length/Length','Pitch/Length');
legend('Open Loop L-to-L','Closed Loop L-to-L');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
grid on
% plot_darkmode
%% Plot Length Input
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(fL,MagL2Y,'-.','LineWidth',3); %L to L Experimental
% loglog(fL,MagL2P*1/ct2N,'-.','LineWidth',3); %L to P Experimental
% loglog(wModelInF/2/pi,squeeze(MagModelInF),'LineWidth',3); % L to L Model
hold on
% loglog(fL,MagL2P,'-.','LineWidth',3); %L to P Experimental
% loglog(woutL/2/pi,normalize(MagSimL,'norm'),'LineWidth',3); %L to L Simulation
% loglog(wModelInFL/2/pi,squeeze(MagModelInFL),'LineWidth',3); % L to L Model
% loglog(wModelInT/2/pi,squeeze(MagModelInT),'LineWidth',3); % L to P Model
xlim([0.1,10]);
% title('Input Length to Top Mass')
title('Comparison of State Space Model and Experimental Data Top Mass - L-to-P');
% ylabel('Magnitude (\mum/counts, \murad/counts)')
ylabel('Magnitude (\phi/F)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
grid on;
% plot_darkmode


subplot(2,1,2);
% semilogx(fL,PhL2L,'-.','LineWidth',3); % L to L Experimental
semilogx(fL,PhL2Y,'LineWidth',3); % L to P Experimental 
hold on;
% semilogx(fL,PhL2P,'-.','LineWidth',3); % L to P Experimental 
% semilogx(woutL/2/pi,PhSimL,'LineWidth',3); %L to L Simulation
% semilogx(wModelInFL/2/pi,squeeze(PhModelInFL),'LineWidth',3); % L to L Model
% semilogx(wModelInT/2/pi,squeeze(PhModelInT)-360,'LineWidth',3); % L to P Model
grid on;
ylim([-800,400]);
xlim([0.1,10]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
% legend('Length/Length','Pitch/Length');
legend('L-P Experimental','L-P State Space Model');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
% plot_darkmode

%% All length input
figure;
subplot(2,1,1);
loglog(fL,MagL2L);
hold on
loglog(fL,MagL2P);
% loglog(fL,MagL2R);
% loglog(fL,MagL2T);
% loglog(fL,MagL2V);
% loglog(fL,MagL2Y);
title('Input Length to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
xlim([0.1,70])
% plot_darkmode

subplot(2,1,2);
semilogx(fL,PhL2L);
hold on;
semilogx(fL,PhL2P);
% semilogx(fL,PhL2R);
% semilogx(fL,PhL2T);
% semilogx(fL,PhL2V);
% semilogx(fL,PhL2Y);
ylim([-1000,200]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
xlim([0.1,70])
grid on;
% legend('L-to-L','L-to-P','L-to-R','L-to-T','L-to-V','L-to-Y');
legend('L-to-L','L-to-P');
% plot_darkmode;
%% Magnitude Only
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1);
loglog(fL,MagL2P,'-.','LineWidth',3); %L to P Experimental
hold on
loglog(wModelInF/2/pi,squeeze(MagModelInF),'LineWidth',3); % L to P Model
loglog(wModelInT/2/pi,squeeze(MagModelInT),'LineWidth',3); % L to P Model
xlim([0.1,10]);
% title('Input Length to Top Mass')
title('Comparison of State Space Model and Experimental Data Top Mass - L-to-P');
% ylabel('Magnitude (\mum/counts, \murad/counts)')
ylabel('Magnitude (\phi/F)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
% set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
grid on;
xlabel('Frequency (Hz)')

subplot(2,1,2);
% semilogx(fL,PhL2L,'-.','LineWidth',3); % L to L Experimental
semilogx(fL,PhL2P,'LineWidth',3); % L to P Experimental 
hold on;
semilogx(wModelInF/2/pi,squeeze(PhModelInF),'LineWidth',3); % L to P Model
semilogx(wModelInT/2/pi,squeeze(PhModelInT)-360,'LineWidth',3); % L to P Model
grid on;
ylim([-800,400]);
xlim([0.1,10]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
% legend('Length/Length','Pitch/Length');
legend('L-P Experimental','L-P State Space Model');
set(findall(gcf,'-property','FontSize'),'FontSize',20)

legend('Force-to-P Experimental Model','Force-to-P State Space Model','Torque-to-P State Space Model');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
plot_darkmode
%% L to P Bottom Mass Mag Only
figure;
% subplot(2,1,1);
% loglog(fL,squeeze(MagL2PS1),'-.','LineWidth',3); %L to PS Experimental
loglog(fL,MagL2OpP,'LineWidth',3); %L to Op Lev P Experimental
hold on
% loglog(fL,MagL2OpY,'LineWidth',3); %L to Op Lev Y Experimental
% loglog(fL,squeeze(MagL2OpP),'-.','LineWidth',3); %L to Op Lev P Experimental
% loglog(wModelLLB/2/pi, normalize(MagModelLLB,'norm'), 'LineWidth',3);
% loglog(wModelInFL/2/pi, squeeze(MagModelInFL), 'LineWidth',3);
loglog(wModelInF/2/pi, squeeze(MagModelInF).*131.62, 'LineWidth',3);
xlim([0.1,10]);
% title('Input Length to Top Mass')
title('Length Input to Bottom Mass Comparison - L-to-P');
ylabel('Magnitude (\murad/counts)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
legend('Experimental d = 2 mm','State Space Model d = 2mm');
grid on;
xlabel('Frequency (Hz)')
plot_darkmode
%% L to Output Bottom Mass
figure;
subplot(2,1,1);
loglog(fL,squeeze(MagL2PS1),'-.','LineWidth',3); %L to PS Experimental
% loglog(fL,normalize(MagL2OpP, 'norm'),'LineWidth',3); %L to Op Lev P Experimental
hold on
% loglog(fL,MagL2OpY,'LineWidth',3); %L to Op Lev Y Experimental
loglog(fL,squeeze(MagL2OpP),'-.','LineWidth',3); %L to Op Lev P Experimental
% loglog(wModelLLB/2/pi, normalize(MagModelLLB,'norm'), 'LineWidth',3);
loglog(wModelInFL/2/pi, squeeze(MagModelInFL), 'LineWidth',3);
loglog(wModelInF/2/pi, squeeze(MagModelInF), 'LineWidth',3);
xlim([0.1,10]);
% title('Input Length to Top Mass')
title('Length Input to Bottom Mass Comparison');
ylabel('Magnitude (\murad/counts)')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-5 10^-3 10^-1 10^1 10^3 10^5 ])
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fL,squeeze(PhL2PS1),'-.','LineWidth',3); % L to PS Experimental
% semilogx(fL,PhL2OpP,'LineWidth',3); % L to Op Lev P Experimental 
hold on;
% semilogx(fL,PhL2OpY,'LineWidth',3); % L to Op Lev Y Experimental 
semilogx(fL,squeeze(PhL2OpP),'-.','LineWidth',3); % L to Op Lev P Experimental 
% semilogx(wModelLLB/2/pi, PhModelLLB, 'LineWidth',3);
semilogx(wModelInFL/2/pi, squeeze(PhModelInFL), 'LineWidth',3);
semilogx(wModelInF/2/pi, squeeze(PhModelInF), 'LineWidth',3);
grid on;
ylim([-800,400]);
xlim([0.1,10]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
% legend('Length/Length','Pitch/Length');
legend( 'Force L-to-L (Experimental)','Force L-to-P (Experimental)',...
    'Force L-to-L (Model)','Force L-to-P (Model)');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
plot_darkmode


%% My attempt at fitting L-to-L
s = tf('s');
wz = 0.9*2*pi;
zz = .007;
wn1 = 0.7*2*pi;
% z1 = 0.0466;
z1 = 0.019;
wn2 = 1.594*2*pi;
% z2 = 0.02584;
z2 = 0.01;

zero = s^2+2*wz*zz*s+wz^2;
p1 = s^2+2*z1*wn1*s+wn1^2;
p2 = s^2+2*z2*wn2*s+wn2^2;
k = 0.06464/0.01776;

H = k*zero/(p1*p2);


[magfitLL, phfitLL, wfitLL] = bode(H,{0.1*2*pi,10*2*pi});

figure;
subplot(2,1,1);
loglog(fL,MagL2L,'-.','LineWidth',3); %L to L Experimental
hold on
loglog(wfitLL/2/pi,squeeze(magfitLL),'--','LineWidth',3); % L to L fit
xlim([0.1,10]);
title('Experimental and Fitted L-to-L');
ylabel('Magnitude (\mum/counts)')
grid on;

subplot(2,1,2);
semilogx(fL,PhL2L,'-.','LineWidth',3); % L to L Experimental
hold on
semilogx(wfitLL/2/pi,squeeze(phfitLL),'--','LineWidth',3); % L to L fit
ylim([-800,400]);
xlim([0.1,10]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
legend('Experimental','Fit')
grid on;

%% Pitch Input

figure;
subplot(2,1,1);

loglog(fP,MagP2P,'MarkerSize',2);
hold on
loglog(fP,MagP2L,'-.');
loglog(wout/2/pi,MagSim,'-.');
xlim([0.1,30]);
plot_darkmode

title('Input Pitch to Top Mass')
ylabel('Mag [\mum/cts, \murad/cts]')

grid on;
subplot(2,1,2);
semilogx(fP,PhP2P);
hold on;
semilogx(fP,PhP2L);
semilogx(wout/2/pi,PhSim,'-.');
legend('Pitch/Pitch','Length/Pitch','Simulated P/P');
plot_darkmode
grid on;
ylim([-400,300]);
xlim([0.1,30]);

ylabel('Phase [deg]')
xlabel('Frequency [Hz]')
%% Roll Input
figure;
subplot(2,1,1);
loglog(fR,MagR2L);
hold on
loglog(fR,MagR2P);
loglog(fR,MagR2R);
loglog(fR,MagR2T);
loglog(fR,MagR2V);
loglog(fR,MagR2Y);
title('Input Roll to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,20]);
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fR,PhR2L);
hold on;
semilogx(fR,PhR2P);
semilogx(fR,PhR2R);
semilogx(fR,PhR2T);
semilogx(fR,PhR2V);
semilogx(fR,PhR2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('R-to-L','R-to-P','R-to-R','R-to-T','R-to-V','R-to-Y');
plot_darkmode;

%% Roll input comparison
figure;

loglog(fR,MagR2R,'LineWidth',3);
hold on
loglog(wModelTR/2/pi,squeeze(MagModelTR),'LineWidth',3);
%% Trans Input
figure;
subplot(2,1,1);
loglog(fT,MagT2L);
hold on
loglog(fT,MagT2P);
loglog(fT,MagT2R);
loglog(fT,MagT2T);
loglog(fT,MagT2V);
loglog(fT,MagT2Y);
title('Input Transverse to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
xlim([0.1,20]);
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fT,PhT2L);
hold on;
semilogx(fT,PhT2P);
semilogx(fT,PhT2R);
semilogx(fT,PhT2T);
semilogx(fT,PhT2V);
semilogx(fT,PhT2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('T-to-L','T-to-P','T-to-R','T-to-T','T-to-V','T-to-Y');
plot_darkmode;

%% Vert Input
figure;
subplot(2,1,1);
loglog(fV,MagV2L);
hold on
loglog(fV,MagV2P);
loglog(fV,MagV2R);
loglog(fV,MagV2T);
loglog(fV,MagV2V);
loglog(fV,MagV2Y);
title('Input Vertical to Top Mass')
xlim([0.1,20]);
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
plot_darkmode

subplot(2,1,2);
semilogx(fV,PhV2L);
hold on;
semilogx(fV,PhV2P);
semilogx(fV,PhV2R);
semilogx(fV,PhV2T);
semilogx(fV,PhV2V);
semilogx(fV,PhV2Y);
ylim([-1000,200]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('V-to-L','V-to-P','V-to-R','V-to-T','V-to-V','V-to-Y');
plot_darkmode;

%% Yaw Input

figure;
subplot(2,1,1);
loglog(fY,MagY2L);
hold on
loglog(fY,MagY2P);
loglog(fY,MagY2R);
loglog(fY,MagY2T);
loglog(fY,MagY2V);
loglog(fY,MagY2Y);
xlim([0.1,20]);
title('Input Yaw to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;
% plot_darkmode

subplot(2,1,2);
semilogx(fY,PhY2L);
hold on;
semilogx(fY,PhY2P);
semilogx(fY,PhY2R);
semilogx(fY,PhY2T);
semilogx(fY,PhY2V);
semilogx(fY,PhY2Y);
ylim([-800,400]);
xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('Y-to-L','Y-to-P','Y-to-R','Y-to-T','Y-to-V','Y-to-Y');

% plot_darkmode;
%%
sys2 = ss(Ay,By,Cy,Dy);
[MagModelYY, PhModelYY, wModelYY] = bode(sys2(1,1),{0.1,1000}); %In Yaw, out Yaw top mass
figure
subplot(2,1,1);
loglog(fY,19.2399*MagY2Y,'LineWidth',3);
hold on
loglog(wModelYY/2/pi,squeeze(MagModelYY),'LineWidth',3);
% xlim([0.1,20]);
title('Input Yaw to Top Mass')
ylabel('Magnitude (\mum/cts, \murad/cts)')
grid on;

subplot(2,1,2);
semilogx(fY,PhY2Y,'LineWidth',3);
hold on
loglog(wModelYY/2/pi,squeeze(PhModelYY),'LineWidth',3);
ylim([-800,400]);
% xlim([0.1,20]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
grid on;
legend('Y-to-Y experimental','Y-to-Y Model')
%% Diagonal TFs
figure;
subplot(2,1,1);
loglog(fL,MagL2L);
hold on;
loglog(fP, MagP2P);
loglog(fR, MagR2R);
loglog(fT, MagT2T);
loglog(fY, MagY2Y);
loglog(fV, MagV2V);
xlim([0.1,60]);
title('Diagonal Transfer Functions');
ylabel('Magnitude')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(gca, 'YTick', [10^-3 10^-1 10^1 10^3 ])
set(findall(gcf,'type','line'),'linewidth',3)
grid on;

subplot(2,1,2);
semilogx(fL, PhL2L);
hold on
semilogx(fP, PhP2P);
semilogx(fR, PhR2R);
semilogx(fT, PhT2T);
semilogx(fY, PhY2Y);
semilogx(fV, MagV2V);
grid on;
ylim([-800,400]);
xlim([0.1,60]);
ylabel('Phase (deg)')
xlabel('Frequency (Hz)')
legend( 'L-to-L','P-to-P','R-to-R','T-to-T','Y-to-Y','V-to-V');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
set(findall(gcf,'type','line'),'linewidth',3)